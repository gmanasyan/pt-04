package ru.manasyan.patterns.adapter.repository.user.orm.second;

/**
 * ISecondOrm.
 *
 * @author Ilya_Sukhachev
 */
public interface ISecondOrm {

    ISecondOrmContext getContext();
}
