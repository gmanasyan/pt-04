package ru.manasyan.patterns.adapter.repository.user.orm.second;

import ru.manasyan.patterns.adapter.repository.user.DbUserEntity;
import ru.manasyan.patterns.adapter.repository.user.DbUserInfoEntity;
import ru.manasyan.patterns.adapter.repository.user.orm.db.SharedDatabase;

import java.util.Set;
import java.util.stream.Collectors;

public class SecondOrmContextStubImpl implements ISecondOrmContext {

    private final SharedDatabase db = SharedDatabase.getInstance();

    @Override
    public Set<DbUserEntity> getUsers() {
        return db.getDb().entrySet().stream()
                .filter(x -> x.getValue() instanceof DbUserEntity)
                .map(x -> (DbUserEntity) x.getValue())
                .collect(Collectors.toSet());
    }

    @Override
    public Set<DbUserInfoEntity> getUserInfos() {
        return db.getDb().entrySet().stream()
                .filter(x -> x instanceof DbUserInfoEntity)
                .map(x -> (DbUserInfoEntity) x)
                .collect(Collectors.toSet());
    }

}
