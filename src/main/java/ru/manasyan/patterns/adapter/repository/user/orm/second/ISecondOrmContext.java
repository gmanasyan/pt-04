package ru.manasyan.patterns.adapter.repository.user.orm.second;


import ru.manasyan.patterns.adapter.repository.user.DbUserEntity;
import ru.manasyan.patterns.adapter.repository.user.DbUserInfoEntity;

import java.util.Set;

/**
 * ISecondOrmContext.
 *
 * @author Ilya_Sukhachev
 */
public interface ISecondOrmContext {

    Set<DbUserEntity> getUsers();
    Set<DbUserInfoEntity> getUserInfos();

}
