package ru.manasyan.patterns.adapter.repository;

import java.time.LocalDateTime;

public class User {

    private Long id;
    private String login;
    private String password;
    private String name;
    private LocalDateTime birthday;

    public User(Long id, String login, String password, String name, LocalDateTime birthday) {
        this.id = id;
        this.login = login;
        this.password = password;
        this.name = name;
        this.birthday = birthday;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public LocalDateTime getBirthday() {
        return birthday;
    }

    public void setBirthday(LocalDateTime birthday) {
        this.birthday = birthday;
    }
}
