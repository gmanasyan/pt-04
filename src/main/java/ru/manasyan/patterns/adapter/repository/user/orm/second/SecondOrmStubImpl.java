package ru.manasyan.patterns.adapter.repository.user.orm.second;

import static java.util.Objects.isNull;

public class SecondOrmStubImpl implements ISecondOrm {

    private ISecondOrmContext secondOrmContext;

    @Override
    public ISecondOrmContext getContext() {
        if (isNull(secondOrmContext)) {
            secondOrmContext = new SecondOrmContextStubImpl();
        }
        return secondOrmContext;
    }
}
