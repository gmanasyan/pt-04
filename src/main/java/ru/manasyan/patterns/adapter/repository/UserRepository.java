package ru.manasyan.patterns.adapter.repository;

import java.util.Collection;

public interface UserRepository {

    void create(User user);

    User read(Long id);

    Iterable<User> readAll(Collection<Long> userIds);

    Iterable<User> readAll();

    void update(User user) throws Exception;

    void delete(Long id);

}
