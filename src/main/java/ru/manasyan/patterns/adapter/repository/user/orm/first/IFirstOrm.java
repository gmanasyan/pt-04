package ru.manasyan.patterns.adapter.repository.user.orm.first;

import ru.manasyan.patterns.adapter.repository.user.IDbEntity;

/**
 * IFirstOrm.
 *
 * @author Ilya_Sukhachev
 */
public interface IFirstOrm<T extends IDbEntity> {

    void create(T entity);

    T read(int id);

    void update(T entity);

    void delete(T entity);
}
