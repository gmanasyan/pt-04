package ru.manasyan.patterns.adapter.repository.user.orm.first;

import ru.manasyan.patterns.adapter.repository.user.IDbEntity;
import ru.manasyan.patterns.adapter.repository.user.orm.db.SharedDatabase;

import java.util.HashMap;
import java.util.Map;

public class FirstOrmStubImpl<T extends IDbEntity> implements IFirstOrm<T> {

    private final SharedDatabase db = SharedDatabase.getInstance();

    @Override
    public void create(T entity) {
        System.out.println("DB - Create " + entity.getId());
        db.getDb().put(entity.getId(), entity);
    }

    @Override
    public T read(int id) {
        System.out.println("DB - Read " + id);
        return (T) db.getDb().get((long) id);
    }

    @Override
    public void update(T entity) {
        System.out.println("DB - Update " + entity.getId());
        db.getDb().put(entity.getId(), entity);
    }

    @Override
    public void delete(T entity) {
        System.out.println("DB - Delete " + entity.getId());
        db.getDb().remove(entity.getId());
    }
}
