package ru.manasyan.patterns.adapter.repository.user;

/**
 * IDbEntity.
 *
 * @author Ilya_Sukhachev
 */
public interface IDbEntity {

    Long getId();

    void setId(Long id);
}
