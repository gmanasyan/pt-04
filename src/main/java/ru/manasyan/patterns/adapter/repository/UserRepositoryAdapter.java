package ru.manasyan.patterns.adapter.repository;

import ru.manasyan.patterns.adapter.repository.user.DbUserEntity;
import ru.manasyan.patterns.adapter.repository.user.DbUserInfoEntity;
import ru.manasyan.patterns.adapter.repository.user.orm.db.SharedDatabase;
import ru.manasyan.patterns.adapter.repository.user.orm.first.FirstOrmStubImpl;
import ru.manasyan.patterns.adapter.repository.user.orm.first.IFirstOrm;
import ru.manasyan.patterns.adapter.repository.user.orm.second.ISecondOrm;
import ru.manasyan.patterns.adapter.repository.user.orm.second.SecondOrmStubImpl;

import java.time.LocalDateTime;
import java.util.Collection;
import java.util.Set;
import java.util.stream.Collectors;

import static java.util.Objects.isNull;

public class UserRepositoryAdapter implements UserRepository {

    private final IFirstOrm<DbUserEntity> userOrm = new FirstOrmStubImpl<>();
    private final IFirstOrm<DbUserInfoEntity> infoOrm = new FirstOrmStubImpl<>();
    private final ISecondOrm getAllOrm = new SecondOrmStubImpl();
    private final SharedDatabase db = SharedDatabase.getInstance();

    @Override
    public void create(User user) {
        Long infoId = createOrUpdateUserInfo(user);

        Long userId = getUserId(user.getLogin());
        if (isNull(userId)) {
            userId = db.getNextId();
        }

        DbUserEntity dbUser = new DbUserEntity(userId, user.getLogin(), user.getPassword(), infoId);
        userOrm.create(dbUser);
    }

    @Override
    public void update(User user) throws Exception {
        if (isNull(userOrm.read(user.getId().intValue()))) {
            throw new Exception("No such user to update");
        }
        Long infoId = createOrUpdateUserInfo(user);
        DbUserEntity dbUser = new DbUserEntity(user.getId(), user.getLogin(), user.getPassword(), infoId);
        userOrm.update(dbUser);
    }

    @Override
    public User read(Long userId) {
        DbUserEntity user = userOrm.read(userId.intValue());
        if (isNull(user)) return null;

        DbUserInfoEntity userInfo = infoOrm.read(user.getUserInfoId().intValue());

        return new User(user.getId(), user.getLogin(), user.getPassword(),
                userInfo.getName(), userInfo.getBirthday());
    }

    @Override
    public Iterable<User> readAll(Collection<Long> userIds) {
        return userIds.stream().map(this::read).collect(Collectors.toList());
    }

    @Override
    public Iterable<User> readAll() {
        Set<DbUserEntity> users = getAllOrm.getContext().getUsers();
        return users.stream().map(x -> {
            DbUserInfoEntity userInfo = infoOrm.read(x.getUserInfoId().intValue());
            return new User(x.getId(), x.getLogin(), x.getPassword(),
                    userInfo.getName(), userInfo.getBirthday());
        }).collect(Collectors.toSet());
    }

    @Override
    public void delete(Long userId) {
        DbUserEntity user = userOrm.read(userId.intValue());
        if (isNull(user)) return;
        DbUserInfoEntity userInfo = infoOrm.read(user.getUserInfoId().intValue());

        infoOrm.delete(userInfo);
        userOrm.delete(user);
    }

    private Long getUserId(String login) {
        Set<DbUserEntity> users = getAllOrm.getContext().getUsers();
        for (DbUserEntity user : users) {
            if (user.getLogin().equals(login)) {
                return user.getId();
            }
        }
        return null;
    }

    private Long createOrUpdateUserInfo(User user) {
        Long infoId = getInfoId(user.getName(), user.getBirthday());
        if (isNull(infoId)) {
            infoId = db.getNextId();
        }
        DbUserInfoEntity dbUserInfo = new DbUserInfoEntity(infoId, user.getName(), user.getBirthday());

        if (isNull(infoOrm.read(infoId.intValue()))) {
            infoOrm.create(dbUserInfo);
        } else {
            infoOrm.update(dbUserInfo);
        }
        return infoId;
    }

    private Long getInfoId(String userName, LocalDateTime birthday) {
        Set<DbUserInfoEntity> userInfos = getAllOrm.getContext().getUserInfos();
        for (DbUserInfoEntity userInfo : userInfos) {
            if (userInfo.getName().equals(userName) &&
                    userInfo.getBirthday().equals(birthday)) {
                return userInfo.getId();
            }
        }
        return null;
    }

}
