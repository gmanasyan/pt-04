package ru.manasyan.patterns.adapter.repository.user.orm.db;

import java.util.HashMap;
import java.util.Map;

import static java.util.Objects.isNull;

public class SharedDatabase {

    private Map<Long, Object> db = new HashMap<>();

    private static SharedDatabase instance = null;

    private SharedDatabase() {
    }

    public static SharedDatabase getInstance() {
        if (isNull(instance)) {
            instance = new SharedDatabase();
        }
        return instance;
    }

    public Map<Long, Object> getDb() {
        return db;
    }

    public Long getNextId() {
        return (long) db.size() + 1;
    }
}
