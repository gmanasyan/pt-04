package ru.manasyan.patterns.adapter;

import ru.manasyan.patterns.adapter.repository.User;
import ru.manasyan.patterns.adapter.repository.UserRepository;
import ru.manasyan.patterns.adapter.repository.UserRepositoryAdapter;

import java.time.LocalDateTime;

public class Main {

    public static void main(String[] args) {

        UserRepository userRepository = new UserRepositoryAdapter();

        userRepository.create(new User(null, "N_O_M_A_D", "Arwen777",
                "Aragorn", LocalDateTime.of(2931, 3, 1, 12, 00)));

        userRepository.create(new User(null, "hobbit", "ring",
                "Frodo Baggins", LocalDateTime.of(4094, 9, 22, 12, 00)));

        userRepository.readAll().forEach(x -> printUser(x));

        userRepository.delete(2L);

        userRepository.readAll().forEach(x -> printUser(x));

    }

    private static void printUser(User x) {
        System.out.println("===== UserEntity =====");
        System.out.println("=== id          : " + x.getId());
        System.out.println("=== login       : " + x.getLogin());
        System.out.println("=== password    : " + x.getPassword());
        System.out.println("=== name        : " + x.getName());
        System.out.println("=== birthday    : " + x.getBirthday());
        System.out.println();
    }

}
